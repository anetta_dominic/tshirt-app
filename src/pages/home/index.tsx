import React, { Fragment, useEffect, useState } from 'react';
import Search from "../../components/search"
import './style.css'
import Filterbar from '../../components/filter-bar';
import { filterContent } from './helper';
import Card from '../../components/card';
import { useDispatch, useSelector } from 'react-redux';
import { fetchData } from '../../store/action'
import { CardProps,FilterProps } from './types';
import Alert from '../../components/alert';


const Home = () => {
    const dispatch = useDispatch();
    const { data, loading, error } = useSelector((state: any) => state);
    let {cartCount}= useSelector((state: any) => state);
    console.log("cartCount",cartCount)
    const [content, setContent] = useState([])
    const [showAlert, setShowAlert] = useState(false)
    const [filters,setFilters]=useState<any>({
        gender: [], 
        color: [], 
        priceRange: [{ min: 0, max: 450 }], 
        type: [],
      })
    
    useEffect(() => {
        dispatch(fetchData());
    }, [dispatch]);

    useEffect(() => {
        setContent(data)
    }, [data]);

    if (loading) {
        return <div>Loading...</div>;
    }

    if (error) {
        return <div>Error: {error}</div>;
    }

    const searchItems=( searchText: any)=> {
        const searchTextLower = searchText.toLowerCase();
        const results = data?.filter((item: any) => {
            const itemNameLower = item.name.toLowerCase();
            const itemColorLower = item.color.toLowerCase();

            return itemNameLower.includes(searchTextLower) || itemColorLower.includes(searchTextLower);
        });
        setContent(results)
    }

      
const filteredProducts = data?.filter((product:any) => {
    const genderMatch = filters.gender.length === 0 || filters.gender.includes(product.gender);
    const colorMatch = filters.color.length === 0 || filters.color.includes(product.color);
    const priceMatch =
      filters.priceRange.length === 0 ||
      filters.priceRange.some(
        (range:any) => product.price >= range.min && product.price <= range.max
      );
    const typeMatch = filters.type.length === 0 || filters.type.includes(product.type);
  
    return genderMatch || colorMatch || priceMatch ||typeMatch;
  });
  
  const handleFilterAction = (filter: string, value: string) => {
    switch (filter) {
      case "Color":
      case "Gender":
      case "Type":
        setFilters((prevFilters:any) => ({
          ...prevFilters,
          [filter?.toLowerCase()]:  [...prevFilters[filter.toLowerCase()], value],
        }));
        break;
      case "Price":
        // Handle price range logic if needed
        break;
      default:
        break;
    }
  };
  console.log("filteredProducts",filteredProducts);
      console.log("filter",filters)
   
console.log("content",content)

const addToCart = (id: number) => {
    const updatedContent = content.map((item: CardProps) => {
      if (item.id === id) {
        // Reduce quantity by 1
        item.quantity -= 1;

        // Optionally, remove the item if quantity becomes 0
        if (item.quantity === 0) {
            setShowAlert(true)
          return null; // or use a special value to filter later
        }
        else{
            console.log("enter")
          cartCount++
        }
      }
      return item;
    });

    // Filter out items with quantity 0
    const filteredContent:any = updatedContent.filter((item) => item !== null);

    // Update the state with the new content
    setContent(filteredContent);
  };

  const handleAlert=()=>{
    setShowAlert(!showAlert)
  }

  return (
        <Fragment>
            <Alert show={showAlert} handleAlert={handleAlert} message={'The Selected Product is currently out of stock'}/>
            <div className="container">
                <div className='row my-4'>
                    <div className="col-md-7">
                        <Search searchItems={searchItems} showFilter={true} placeholderContent='Search for products...' />
                    </div>
                </div>
            </div>
            <div className="container">
                <Filterbar onFilterChange ={handleFilterAction} data={filterContent} />
                <div className="content">
                    <div className="row">
                        {content?.length>0 ? content?.map((item: CardProps) => (
                            <div className="col-12 col-md-4" key={item.id}>
                                <Card data={item} handleCart={addToCart}/>
                            </div>
                        )): <>No Data Found</>}
                    </div>
                </div>
            </div>
        </Fragment>
    )
}


export default Home