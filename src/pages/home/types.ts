export type CardProps = {
    color: string;
    currency:string;
    gender:string;
    id: number;
    imageURL:string;
    name:string;
    price:number;
    quantity:number;
    type: string;
}

export type FilterProps= {
    gender: string[];
    color: string[];
    price: { min: number; max: number }[];
    type: string[];
  }