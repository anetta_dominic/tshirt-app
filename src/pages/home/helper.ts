export const filterContent = [{
    criteria: "Color",
    values: ["Red", "Blue", "Green"]
}, {
    criteria: "Gender",
    values: ["Men", "Women",]
}, {
    criteria: "Price",
    values: ["0-Rs250", "Rs 251- Rs 450", "Rs 450 above" ]
},{
    criteria: "Type",
    values: ["Polo", "Hoodie", "Basic"]
}]