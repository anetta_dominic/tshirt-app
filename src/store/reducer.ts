// src/store/reducers.ts

import { ActionTypes, Action } from './action';

interface AppState {
  data: any; 
  loading: boolean;
  error: string | null;
}

const initialState: AppState = {
  data: null,
  loading: false,
  error: null,
};

const rootReducer = (state = initialState, action: Action): AppState => {
  switch (action.type) {
    case ActionTypes.FETCH_DATA_REQUEST:
      return { ...state, loading: true, error: null };
    case ActionTypes.FETCH_DATA_SUCCESS:
      return { ...state, loading: false, data: action.payload };
    case ActionTypes.FETCH_DATA_FAILURE:
      return { ...state, loading: false, error: action.error };
    default:
      return state;
  }
};

export default rootReducer;
