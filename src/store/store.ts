// src/store/store.ts

import { legacy_createStore as createStore ,applyMiddleware, Middleware} from 'redux';
import thunk, { ThunkMiddleware } from 'redux-thunk';
import rootReducer from './reducer';

// Define your root state type
type RootState = ReturnType<typeof rootReducer>;

// Create a type for the thunk middleware
type ThunkAction = ThunkMiddleware<RootState, any>;

const store = createStore(
  rootReducer,
  applyMiddleware(thunk as unknown as ThunkAction)
);

export default store;
