import { Dispatch } from 'redux';
import axios from 'axios';

export enum ActionTypes {
  FETCH_DATA_REQUEST = 'FETCH_DATA_REQUEST',
  FETCH_DATA_SUCCESS = 'FETCH_DATA_SUCCESS',
  FETCH_DATA_FAILURE = 'FETCH_DATA_FAILURE',
  CART_COUNT='CART_COUNT'
}

interface FetchDataRequest {
  type: ActionTypes.FETCH_DATA_REQUEST;
}

interface FetchDataSuccess {
  type: ActionTypes.FETCH_DATA_SUCCESS;
  payload: any;
}

interface FetchDataFailure {
  type: ActionTypes.FETCH_DATA_FAILURE;
  error: string;
}


export type Action = FetchDataRequest | FetchDataSuccess | FetchDataFailure;

export const fetchData = () => {
  return async (dispatch: Dispatch<Action>) => {
    dispatch({ type: ActionTypes.FETCH_DATA_REQUEST });

    try {
      const response = await axios.get('https://geektrust.s3.ap-southeast-1.amazonaws.com/coding-problems/shopping-cart/catalogue.json');
      dispatch({
        type: ActionTypes.FETCH_DATA_SUCCESS,
        payload: response.data,
      });
    } catch (error) {
      dispatch({
        type: ActionTypes.FETCH_DATA_FAILURE,
        error: 'Failed to fetch data',
      });
    }
  };
};

export const handleCartCount=()=>{

}
