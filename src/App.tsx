
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Home from "./pages/home";
import Header from "./components/header";
import { Provider } from 'react-redux';
import store from './store/store';
import 'bootstrap/dist/css/bootstrap.min.css';
import Cart from "./pages/cart";


function App() {
  return (
    <div className="App">
       <Provider store={store}>
    <Router>
      <Header/>
    <Routes>
    <Route path="/" element={<Home />} />
    <Route path="/cart" element={<Cart/>} />
    </Routes>
    </Router>
    </Provider>
    </div>
  );
}

export default App;
