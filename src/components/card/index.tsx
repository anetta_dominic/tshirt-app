import React from 'react';
import {CardProps } from './type';
import "./style.css"

const Card: React.FC <CardProps>= ({data,handleCart}) => {
  
    const handleClick = () => {
        handleCart(data?.id);
      };

    return (
        <div className="card">
        <img src={data?.imageURL} alt="Product Image"/>
        <div className="card-content">
          <div className="card-title">{data?.name}</div>
          <div style={{display:'flex'}}>
          <div className="card-price">{data?.currency}{data?.price}/-</div>
          <button className="card-btn" onClick={handleClick}>Add to Cart</button>
          </div>
        </div>
      </div>
    );
};

export default Card;

