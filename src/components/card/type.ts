export type DataProps = {
    color:
    string;
    currency
    :
    string;
    gender
    :
    string;
    id
    :
    number;
    imageURL
    :
    string;
    name
    :
    string;
    price
    :
    number;
    quantity
    :
    number;
    type
    :
    string;
}
export type CardProps={
data: DataProps ,
handleCart:(id:number)=>any
}
