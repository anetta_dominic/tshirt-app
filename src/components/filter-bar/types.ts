export type FilterbarProps = {
    data: DataProps[];
    onFilterChange:(filter:any,value:any)=>any
};

type DataProps = {
    criteria: string;
    values: string[];
};
