import React from 'react';
import { FilterbarProps } from './types';
import "./style.css"

const Filterbar: React.FC<FilterbarProps> = ({ data,onFilterChange }) => {
    const handleCheckboxChange = (filter: string, value: string) => {
        onFilterChange(filter, value);
      };

   
    return (
        <div className="filterbar">
            {data?.map((item) => (
                <div key={item.criteria}>
                    <h6>{item.criteria}</h6>
                    {item.values.map((value) => (
                        <label key={value} className='filter-label'>
                            <input type="checkbox"  onChange={() => handleCheckboxChange(item.criteria, value)}/>
                            {value}
                        </label>
                    ))}
                </div>
            ))}
        </div>
    );
};

export default Filterbar;

