import React from 'react';
import "./style.css"

const Header: React.FC = () => {
  return (
    <nav>
      <div className='brand-name'> TeeRex Store</div>
      <div className='nav-contents'>
        <a href="/">Products</a>

        <a href='/cart'>
          <div className="cart-icon" >
            <img src='images/cart.svg' />
            {/* <span className='cart-count'><sup>1</sup></span> */}
          </div>
        </a>
      </div>
    </nav>
  );
};

export default Header;
