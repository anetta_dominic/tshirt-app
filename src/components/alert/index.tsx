import React, { Fragment } from 'react';
import "./style.css"

type AlertProps = {
    show: boolean
    handleAlert:()=>any
    message:string
}

const Alert: React.FC<AlertProps> = ({ show, handleAlert,message }) => {
    return (
       show &&
        <div className="alert-wrap">
            <span className="closebtn" onClick={handleAlert}>&times;</span>
            {message}
        </div>
    );
};

export default Alert;
