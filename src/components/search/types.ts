export type SearchProps={
    showFilter?: Boolean;
    placeholderContent:string|'';
    searchItems:( searchText: any) => any;
}