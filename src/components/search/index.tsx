import React, { useState } from 'react';
import FilterButton from '../filter-btn';
import { SearchProps } from './types';
import './style.css';

const Search: React.FC<SearchProps> = ({ showFilter = false, placeholderContent = '', searchItems }) => {
  const [showFilterbar, setFilterbar] = useState(false);
  const [searchInput, setSearchInput] = useState('');

  const handleSearchInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchInput(event.target.value);
  };

  const handleSearchButtonClick = () => {
    searchItems(searchInput);
  };

  return (
    <div className="search-container">
      <input
        type="text"
        className="search-box"
        placeholder={placeholderContent}
        value={searchInput}
        onChange={handleSearchInputChange}
      />
      <button className="search-button" onClick={handleSearchButtonClick}>
        <img src="./images/search.svg" alt="Search" />
      </button>
      {showFilter && <FilterButton />}
    </div>
  );
};

export default Search;
